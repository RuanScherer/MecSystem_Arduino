#include <SPI.h>
#include <MFRC522.h>
#include <Ethernet.h>
#include <MySQL_Connection.h>
#include <MySQL_Cursor.h>
//Pinos
#define LED_VERDE 6
#define LED_VERMELHO 7
#define BUZZER 5
#define SS_PIN 8
#define RST_PIN 9

#define LM35 A0

char sentenca[128];
byte mac_addr[] = { 0xDE, 0xAD, 0xBE, 0xEF, 0xFE, 0xED };

IPAddress server_addr(157, 245, 83, 188);
char user[] = "mecsystem";
char password[] = "izidoro10cm";

//char INSERIR[] = "INSERT INTO `hcode`.`users` (`name`, `password`) VALUES (%s, 'TESTE');";
char BANCODEDADOS[] = "USE BD_MecSystem";

String IDtag = ""; //Variável que armazenará o ID da Tag
bool Permitido = false; //Variável que verifica a permissão
char query[] = "SELECT CardNumber FROM BD_MecSystem.tb_user;";
//Vetor responsável por armazenar os ID's das Tag's cadastradas

MFRC522 LeitorRFID(SS_PIN, RST_PIN);    // Cria uma nova instância para o leitor e passa os pinos como parâmetro
EthernetClient client;
MySQL_Connection conn((Client *)&client);
MySQL_Cursor cur = MySQL_Cursor(&conn);



void setup() {

  SPI.begin();                    // Inicializa comunicacao SPI
  LeitorRFID.PCD_Init();          // Inicializa o leitor RFID
  pinMode(LED_VERDE, OUTPUT);     // Declara o pino do led verde como saída
  pinMode(LED_VERMELHO, OUTPUT);  // Declara o pino do led vermelho como saída
  pinMode(BUZZER, OUTPUT);        // Declara o pino do buzzer como saída
  pinMode(10, OUTPUT);
  pinMode(4, OUTPUT);
  Serial.begin(115200);
  while (!Serial);


}

void loop() {

  digitalWrite(4, HIGH);
  digitalWrite(10, HIGH);
  Leitura();  //Chama a função responsável por fazer a leitura das Tag's
}

void Leitura() {




  IDtag = ""; //Inicialmente IDtag deve estar vazia.
  digitalWrite(10, HIGH);
  // Verifica se existe uma Tag presente
  if ( !LeitorRFID.PICC_IsNewCardPresent() || !LeitorRFID.PICC_ReadCardSerial() ) {
    delay(50);
    return;
  }
  Ethernet.begin(mac_addr);
  if (conn.connect(server_addr, 3306, user, password))
  {
    
    // Pega o ID da Tag através da função LeitorRFID.uid e Armazena o ID na variável IDtag
    for (byte i = 0; i < LeitorRFID.uid.size; i++) {
      IDtag.concat(String(LeitorRFID.uid.uidByte[i], HEX));
    }
    row_values *row = NULL;
      String head_count;

      delay(1000);

            // Initiate the query class instance
      // Execute the query
      MySQL_Cursor *cur_mem = new MySQL_Cursor(&conn);
      cur_mem->execute(query);
      // Fetch the columns (required) but we don't use them.
      column_names *columns = cur_mem->get_columns();

      // Read the row (we are only expecting the one)
      int x = 1;

      do {
        row = cur_mem->get_next_row();
        if (row != NULL) {
          head_count = (row->values[0]);
          Serial.println(x);
          Serial.println(head_count);     
          Serial.println(IDtag);
          x++;
          if (  IDtag.equalsIgnoreCase(head_count)  ) {
            Permitido = true; //Variável Permitido assume valor verdadeiro caso o ID Lido esteja cadastrado
          }
        }
      } while (row != NULL);
    

    if (Permitido == true) {
      acessoLiberado(); //Se a variável Permitido for verdadeira será chamada a função acessoLiberado()

      delay(1000);
      String comando = "select BD_MecSystem.Comparar('";
      comando.concat(IDtag);
      String fimComando = "');";
      comando.concat(fimComando);
      char INSERIR[128];
      comando.toCharArray(INSERIR, 128);
      cur_mem->execute(INSERIR);
      //delete cur_mem; 

      
      // Deleting the cursor also frees up memory used
      delete cur_mem;




    }
    else {
      acessoNegado(); //Se não será chamada a função acessoNegado()
      delete cur_mem;
    }

  } else {
    Serial.println("A conexão falhou");
    conn.close();
  }
  conn.close();
  digitalWrite(4, HIGH);
  digitalWrite(10, HIGH);
  delay(1000); //aguarda 2 segundos para efetuar uma nova leitura
}

void acessoLiberado() {
  efeitoPermitido();  //Chama a função efeitoPermitido()
  Permitido = false;  //Seta a variável Permitido como false novamente




}

void acessoNegado() {
  efeitoNegado(); //Chama a função efeitoNegado()
}

void efeitoPermitido() {
  int qtd_bips = 2; //definindo a quantidade de bips
  for (int j = 0; j < qtd_bips; j++) {
    //Ligando o buzzer com uma frequência de 1500 hz e ligando o led verde.
    tone(BUZZER, 1500);
    digitalWrite(LED_VERDE, HIGH);
    delay(100);

    //Desligando o buzzer e led verde.
    noTone(BUZZER);
    digitalWrite(LED_VERDE, LOW);
    delay(100);
  }
}

void efeitoNegado() {
  int qtd_bips = 1;  //definindo a quantidade de bips
  for (int j = 0; j < qtd_bips; j++) {
    //Ligando o buzzer com uma frequência de 500 hz e ligando o led vermelho.
    tone(BUZZER, 500);
    digitalWrite(LED_VERMELHO, HIGH);
    delay(500);

    //Desligando o buzzer e o led vermelho.
    noTone(BUZZER);
    digitalWrite(LED_VERMELHO, LOW);
    delay(500);
  }
}
